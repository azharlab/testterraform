terraform {
  backend "local" {
    path = "terraform.tfstate"
  }
}
resource "aws_vpc" "terraform-vpc" {
  cidr_block       = "172.16.0.0/16"
  enable_dns_hostnames = true
  tags = {
    Name = "terraform-test-vpc"
  }
}
